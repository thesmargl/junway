FROM debian:latest
RUN apt update && apt install -y apt-transport-https ca-certificates curl software-properties-common gnupg dirmngr
RUN curl -fsSL https://download.docker.com/linux/debian/gpg |  gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt update && apt-get -y install docker-ce docker-ce-cli containerd.io
CMD ["sudo systemctl status docker"]
